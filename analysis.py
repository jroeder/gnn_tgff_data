import os
import sys
sys.path
sys.path.append('/home/julius/Projects/phd/gnn_tgff_data/')
import solve_all#, extract_scheduling_results

solve_all.computed_DAGS('/home/julius/Projects/phd/gnn_tgff_data/smaller/test_ILP_4cores/','/home/julius/Projects/phd/gnn_tgff_data/smaller/test_output/', heuristic=True)
solve_all.computed_DAGS('/home/julius/Projects/phd/gnn_tgff_data/smaller/train_ILP_4cores/','/home/julius/Projects/phd/gnn_tgff_data/smaller/train_output/', heuristic=True)
# solve_all.computed_DAGS('/home/julius/Projects/phd/gnn_tgff_data/larger/test_ILP_4cores/','/home/julius/Projects/phd/gnn_tgff_data/larger/test_output/', heuristic=True)
# solve_all.computed_DAGS('/home/julius/Projects/phd/gnn_tgff_data/larger/train_ILP_4cores/','/home/julius/Projects/phd/gnn_tgff_data/larger/train_output/', heuristic=True)
# extract_scheduling_results.generate_df_from_solve('/home/julius/Projects/phd/tgff_experiments/output/',  files = {0: 'ILP.txt', 1: 'heuristic.txt', 2: 'makespan.txt', 3: 'makespan_single.txt'}, second_dir="/home/julius/Projects/phd/tgff_experiments/rodinia_cross_over_small")


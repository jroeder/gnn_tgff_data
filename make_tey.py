import queue
import os
import math
from typing import Optional, Any
import matplotlib.pyplot as plt

import numpy as np


class taskgraph():
    def __init__(self, text_array, task_graph_num, deadline):
        self.ID = ""
        self.tasks = {}
        self.edges = []
        self.successors = {}
        self.predecessors = {}
        self.task_versions = {}
        self.deadline = deadline

        self.tasks_in_edges = {}
        self.tasks_out_edges = {}

        self.header = ""
        self.footer = ""
        self.components = {}
        self.components_single = {}
        self.utilization = 0
        self.wcets = []

        self.sequential_wcet = 0

        self.init_dag_from_list(text_array, task_graph_num)
        self.make_successors_predecessors()

    def init_dag_from_list(self, text_list, ID):
        self.ID = ID
        for line in text_list:
            split_line = line.split()
            if "TASK " in line:
                self.tasks[split_line[1]] = int(split_line[3])
                self.successors[split_line[1]] = []
                self.predecessors[split_line[1]] = []
                self.task_versions[split_line[1]] = []
                self.tasks_in_edges[split_line[1]] = queue.Queue()
                self.tasks_out_edges[split_line[1]] = queue.Queue()
            elif "ARC" in line:
                self.edges.append([split_line[3], split_line[5]])
            elif "PERIOD" in line:
                self.period = math.ceil(float(split_line[1]))

                # if self.period == 1659:
                #     self.period = 1660
                # elif self.period == 3318:
                #     self.period = 3320
                # elif self.period == 6636:
                #     self.period = 6640
                # elif self.period == 13272:
                #     self.period = 13280

    def make_successors_predecessors(self):
        for task in self.tasks.keys():
            for edge in self.edges:
                if task == edge[0]:
                    self.successors[task].append(edge[1])
                elif task == edge[1]:
                    self.predecessors[task].append(edge[0])

    def get_successors(self, task):
        return self.successors[task]

    def get_predecessors(self, task):
        return self.predecessors[task]

    def generate_coord(self):
        self.header = "app DAG" + str(self.ID) + " { \n"
        # line = line + 'datatypes {\n (int_t, "int")\n}\n components {\n' #for coord
        self.header = self.header + 'components {\n'

        alph = "aaa"
        alph2 = "aaa"
        for t in self.tasks:
            component_line = ""
            component_line = t +" { \n"
            component_line += " deadline=" + str(math.ceil(self.deadline) * 100) + "ns;\n"
            component_line += "period=" + str(math.ceil(self.period) * 100) + "ns;\n"

            num_pre = len(self.predecessors[t])
            num_suc = len(self.successors[t])
            if num_pre > 0:
                component_line = component_line + "inports {"
            for i, p in enumerate(self.get_predecessors(t)):
                self.tasks_in_edges[t].put(".in_" + alph)
                if (i+1 == num_pre):
                    # line = line + "(in_" + alph +",1,int_t)]\n"
                    component_line = component_line + "int in_" + alph + "; \n }\n"
                else:
                    # line = line + "(in_" + alph +",1,int_t)\n"
                    component_line = component_line + "int in_" + alph +"; \n"

                if ord(alph[2]) < 122:
                    alph = alph[0] + alph[1] + chr(ord(alph[2]) + 1)
                elif ord(alph[1]) < 122:
                    alph = alph[0] + chr(ord(alph[1]) + 1) + "a"
                else:
                    alph = chr(ord(alph[0]) + 1) + "aa"

            if num_suc > 0:
                # line = line + "outputs ["#for coord
                component_line = component_line + "outports {"
            for i, s in enumerate(self.get_successors(t)):
                self.tasks_out_edges[t].put(".out_" + alph2)
                if (i + 1 == num_suc):
                    component_line =component_line + "int out_" + alph2 + "; \n }\n"
                else:
                    component_line =  component_line + "int out_" + alph2 +"; \n"

                if ord(alph2[2]) < 122:
                    alph2 = alph2[0] + alph2[1] + chr(ord(alph2[2]) + 1)
                elif ord(alph2[1]) < 122:
                    alph2 = alph2[0] + chr(ord(alph2[1]) + 1) + "a"
                else:
                    alph2 = chr(ord(alph2[0]) + 1) + "aa"

            self.components[t] = component_line #TODO: add closing } to component line
            self.components_single[t] = component_line #TODO: add closing } to component line
            # line = line + "}\n"
        self.footer = "}\n channels { "

        edges = ""
        for e in self.edges:
            edges = edges + e[0] + self.tasks_out_edges[e[0]].get() + "->" + e[1] + self.tasks_in_edges[e[1]].get() + ";\n"

        # self.edges = edges
        self.footer = self.footer + edges + "}\n}"

    def write_tey(self, target_dir, single_version, prefix):
        dag_path = os.path.join(target_dir,"DAG"+str(self.ID+prefix))
        dag_path = os.path.abspath(dag_path)
        os.makedirs(dag_path, exist_ok=True)
        dag_file = os.path.join(dag_path,"DAG"+str(self.ID+prefix)+".tey")
        with open(dag_file, 'w') as dag_f:
            dag_f.writelines(self.header)
            if single_version:
                for c in self.components_single.values():
                    dag_f.writelines(c)
            else:
                for c in self.components.values():
                    dag_f.writelines(c)
            dag_f.writelines(self.footer)

    def write_util(self, target_dir, prefix):
        dag_path = os.path.join(target_dir,"DAG"+str(self.ID+prefix))
        dag_path = os.path.abspath(dag_path)
        os.makedirs(dag_path, exist_ok=True)
        dag_file = os.path.join(dag_path, "utilization.txt")
        with open(dag_file, 'w') as dag_f:
            dag_f.write("Utilization: " + str(self.utilization))

    def parse_feature(self, feature):

        if "little" in feature[0]:
            target = ["armv7little"]
        elif "big" in feature[0]:
            target = ["armv7big"]

        if "little" in feature[0] and "gpu" in feature[0]:
            target = ["armv7little", "gpu"]
        elif "big" in feature[0] and "gpu" in feature[0]:
            target = ["armv7big", "gpu"]

        split_f = feature[1].split()
        type_num_col = -1
        runtime_col = -1
        gpu_percentage_col = -1
        split_col = -1
        for i, col in enumerate(split_f):
            if 'type' in col:
                type_num_col = i - 1
            elif 'runtime' in col:
                runtime_col = i - 1
            elif 'GPU_percentage' in col:
                gpu_percentage_col = i - 1
            elif 'num_phases' in col:
                split_col = i - 1

        types = {}

        # runtime, gpu_percentage, number phases
        for l in feature[2:]:
            split_f = l.split()
            t = int(split_f[type_num_col])
            types[t] = [int(split_f[runtime_col])]

            if gpu_percentage_col > 0:
                types[t].append(float(split_f[gpu_percentage_col]))
            else:
                types[t].append(-1)

            if split_col > 0:
                types[t].append(int(split_f[split_col]))
            else:
                types[t].append(-1)

        return target, types

    def num_phases(self, num):
        remainder = num % 2
        division = num/2
        if remainder == 0:
            return division, division #cpu_phases, gpu_phases
        else:
            return math.ceil(division), math.floor(division)

    def generate_nfp(self, features):
        # np.random.seed(42)

        for ta, ty in self.tasks.items():
            self.components_single[ta] = self.components_single[ta] + "versions { \n"
            self.components[ta] = self.components[ta] + "versions { \n"
            for f in features:
                little_target, types = self.parse_feature(f)
                Little_runtime, gpu_percentage, num_phases = types[ty]
                Little_runtime = Little_runtime
                # Little_runtime = max(10, min(round(np.random.lognormal(4, 0.75)), 150))
                self.sequential_wcet += Little_runtime
                # print(Little_runtime)
                # runtime = runtime*1000 #convert to ms
                # we only generate the LITTLE runtime in tgff and the rest is generated here based on random numbers
                # to create sequential schedule for utilization calculation
                # self.wcets.append(Little_runtime)

                # if (num_phases != -1 and gpu_percentage != -1) or num_phases != 1:
                #     AssertionError("Change tgff file. Generated too many versions in tgff.")

                # Little core
                self.components_single[ta] = self.components_single[ta] + "CPU_Only_"  + little_target[0] +"{ WCET=" + str(round(Little_runtime)) +'ns; \n arch="' + little_target[0] + '"; \n }'
                self.components[ta] = self.components[ta] + "CPU_Only_"  + little_target[0] +"{ WCET=" + str(round(Little_runtime)) +'ns; \n arch="' + little_target[0] + '";\n }'


                # # big core
                # Big_runtime = max(1, Little_runtime * np.random.uniform(0.2, 0.9))
                # self.components[ta] = self.components[ta] + "CPU_Only_armv7big" + "{ \n phases {\n cpu_only { WCET=" + str(round(Big_runtime )) + 'ns; \n arch="armv7big"; \n } \n } \n }'
                #
                # # 3 phase GPU - Little
                # # ensure that if the gpu percentage is small the total runtime cannot be much less than the tgff generated runtime.
                # gpu_percentage = np.random.uniform(0.4, 1)
                # Total_runtime_Little_GPU = Little_runtime * np.random.uniform(0.2, 1)
                # cpu_runtime = max(1, round(Total_runtime_Little_GPU*(1-gpu_percentage)/2))
                # gpu_runtime = max(1, round(Total_runtime_Little_GPU*gpu_percentage))
                # self.components[ta] = self.components[ta] + "GPU_Middle_armv7little" + "{ \n phases { \n first_cpu { \n WCET=" + str(cpu_runtime) + 'ns; \n arch="armv7little"; \n } \n'
                # self.components[ta] = self.components[ta] + "gpu { \n WCET=" + str(gpu_runtime) + 'ns; \n arch="gpu"; \n } \n'
                # self.components[ta] = self.components[ta] + "second_cpu { \n WCET=" + str(cpu_runtime) + 'ns; \n arch="armv7little"; \n \n  } \n } \n  } \n'
                #
                # # 3 phase GPU - Big
                # # The big runtime depends on the little runtime
                # # Only update the CPU runtime
                # cpu_runtime = max(1, round(cpu_runtime * np.random.uniform()))
                # self.components[ta] = self.components[ta] + "GPU_Middle_armv7big" + "{ \n phases { \n first_cpu { \n WCET=" + str(cpu_runtime) + 'ns; \n arch="armv7big"; \n } \n'
                # self.components[ta] = self.components[ta] + "gpu { \n WCET=" + str(gpu_runtime) + 'ns; \n arch="gpu"; \n } \n'
                # self.components[ta] = self.components[ta] + "second_cpu { \n WCET=" + str(cpu_runtime) + 'ns; \n arch="armv7big"; \n \n  } \n } \n  } \n'


            self.components[ta] = self.components[ta] + "}\n" #closes versions
            self.components[ta] = self.components[ta] + "}\n" # closes task

            self.components_single[ta] = self.components_single[ta] + "}\n" #closes versions
            self.components_single[ta] = self.components_single[ta] + "}\n" # closes task

    def calc_utilization(self):
            self.utilization = self.sequential_wcet / self.period

    def generate_config(self):
        pass

    def get_utilization(self, utilization_path: str):

        with open(os.path.join(utilization_path, "DAG" + str(self.ID), "utilization.txt")) as uf:
            self.utilization = float(uf.read().split(" ")[1])

def parse_tgff(file, target_dir, target_dir_single, utilization_path=None, make_multi_graph=False, single_version=False, prefix=0):
    text_array = []
    graph_start = False
    graph_end = False
    feature_set_start = False
    feature_set_end = False
    deadline = 0
    task_graphs = []
    features = []
    feature_num = 0

    with open(file, "r") as dag_file:
        for line in dag_file:
            if "TASK_GRAPH" in line:
                task_graph_num = int(line.split()[1])
                graph_start = True
                graph_end = False
            if graph_start and "}"  in line:
                graph_start = False
                graph_end = True

            if "HARD_DEADLINE" in line and graph_start:
                temp_deadline= int(line.split()[-1])
                deadline = max(temp_deadline, deadline)

            if graph_start:
                text_array.append(line)

            if graph_end:
                task_graphs.append(taskgraph(text_array, task_graph_num, deadline))
                text_array = []
                graph_end = False
                graph_start = False
                deadline = 0

            if "WCET" in line:
                feature_set_start = True
                feature_set_end = False

            if feature_set_start and "}" in line:
                feature_set_start = False
                feature_set_end = True

            if feature_set_end:
                features.append(text_array)
                feature_num = feature_num + 1
                feature_set_end = False
                text_array = []

            if feature_set_start:
                text_array.append(line)
    #
    max_tasks = 0
    avg_task = []
    wcets = []
    for tg in task_graphs:
        avg_task.append(len(tg.tasks))
        if len(tg.tasks) > max_tasks:
            max_tasks = len(tg.tasks)
        tg.generate_coord()
        tg.generate_nfp(features)
        tg.calc_utilization()

    # # try to equalize the utilization
    # bins = range(1, 14, 1)
    # bin_counter = [0] * (len(bins)+1)
    # for tg in task_graphs:
    #     for bin in bins:
    #         if bin == bins[-1]:
    #             bin_counter[bin - 1] += (tg.utilization > (bin - 1))
    #         else:
    #             bin_counter[bin - 1] += (bin >= tg.utilization) & (tg.utilization> (bin - 1))
    #
    # move_it = []
    # total = 0
    # num_above = 0
    # for bin, count in zip(bins, bin_counter):
    #     if count > 80:
    #         total += count
    #         num_above += 1
    #         for tg in task_graphs:
    #             if bin == bins[-1]:
    #                 if(tg.utilization > (bin - 1)):
    #                     move_it.append(tg)
    #             else:
    #                 if((bin >= tg.utilization) & (tg.utilization> (bin - 1))):
    #                     move_it.append(tg)
    #
    # need_moving = (total - 80 * num_above)
    # make_smaller = 10
    # make_larger = need_moving - make_smaller
    # selected = np.random.choice(move_it, size=need_moving, replace=False)
    #
    # # more smaller utilization
    # for i in selected[:make_smaller]:
    #     i.period *= np.random.uniform(1.1, 2)
    #     i.calc_utilization()
    #     i.generate_coord()
    #     i.generate_nfp(features)
    #
    #
    # # more higher utilization
    # for i in selected[make_smaller:]:
    #     i.period *= np.random.uniform(0.1, 0.5)
    #     i.calc_utilization()
    #     i.generate_coord()
    #     i.generate_nfp(features)

    for tg in task_graphs:
        if utilization_path:
            tg.get_utilization(utilization_path)
        tg.write_tey(target_dir, False, prefix)
        # tg.write_util(target_dir_single, prefix)
        print("DAG" + str(tg.ID) + " done")
        # wcets += tg.wcets
    print(max_tasks, np.mean(avg_task))
    # plt.hist(avg_task, bins=40)
    # plt.show()
    # plt.hist(wcets, bins=40)
    # plt.show()


    if make_multi_graph:
        # select random number
        num_combinations = np.random.randint(2, 6, 1000)
        for com in num_combinations:
            # select random task graphs based on the random number
            rand_task_graphs = np.random.choice(task_graphs, com, replace=False)

            taskgraph_string = ""

            utilization = 0
            for tg in rand_task_graphs:
                taskgraph_string += "_" + str(tg.ID)
                utilization += tg.utilization

            header = "app DAG" + taskgraph_string  + " { \n" + 'components {\n'


            dag_path = os.path.join(target_dir, "DAG" + taskgraph_string)
            dag_path = os.path.abspath(dag_path)
            os.makedirs(dag_path, exist_ok=True)
            dag_file = os.path.join(dag_path, "DAG" + taskgraph_string + ".tey")
            with open(dag_file, 'w') as dag_f:
                dag_f.writelines(header)
                for tg in rand_task_graphs:
                    for c in tg.components.values():
                        dag_f.writelines(c)
                dag_f.writelines("}\n channels { ")
                for tg in rand_task_graphs:
                    dag_f.writelines(tg.edges)

                dag_f.writelines("}\n}")

            util_path = os.path.join(utilization_path, "DAG" + taskgraph_string)
            util_path = os.path.abspath(util_path)
            os.makedirs(util_path, exist_ok=True)
            dag_util = os.path.join(util_path, "utilization.txt")
            with open(dag_util, 'w') as dag_util_f:
                dag_util_f.write("Utilization: " + str(utilization))





if __name__ == "__main__":
    # parse_tgff("smaller/test.tgff", "smaller/test_ILP_4cores/", "")
    # parse_tgff("smaller/train.tgff", "smaller/train_ILP_4cores/", "")
    parse_tgff("larger/test.tgff", "larger/test_ILP_4cores/", "")
    parse_tgff("larger/train.tgff", "larger/train_ILP_4cores/", "")
